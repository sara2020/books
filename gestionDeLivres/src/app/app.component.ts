import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'gestionDeLivres';
  constructor(){

    const config={
      apiKey: "AIzaSyCPG0A_meODu6BGYwmcATKPeYDhqc6WnDI",
      authDomain: "gestiondelivres-7059a.firebaseapp.com",
      databaseURL: "https://gestiondelivres-7059a.firebaseio.com",
      projectId: "gestiondelivres-7059a",
      storageBucket: "gestiondelivres-7059a.appspot.com",
      messagingSenderId: "369766183767",
      appId: "1:369766183767:web:35cd626b5bb2efd7028412"
    };
    firebase.initializeApp(config);
    
  }


}
